@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">{{ __('View Product') }}
                    <a href="{{route('home')}}" class="btn btn-primary float-right" role="button">Back</a>

                </div>

                <div class="card-body">
                   <h4><strong>Product Name :</strong> {{$products->name}}</h3>
                   <h4><strong>Product SKU :</strong> {{$products->sku}}</h3>
                    <h4><strong>Created Date :</strong> {{$products->created_at}}</h3>
                    <h4><strong>Created By :</strong> {{$products->user_name->name}}</h3>
                        @php
                            
                            $images = explode("|",$products->image);
                           
                        @endphp
                        @foreach ($images as $item)
                            <img src="{{ asset('images/') }}/{{$item}}" height="250px" width="250px" alt="afafaf">
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(function() {
  $('input[name="datetimes"]').daterangepicker({
    timePicker: false,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
});
    </script>
@endsection