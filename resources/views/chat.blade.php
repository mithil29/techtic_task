<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://use.fontawesome.com/3383758c64.js"></script>
    <!-- Styles -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Chats') }}
                    </div>

                    <div class="card-body">
                        <form action="{{route('filter')}}" method="POST" class="">
                            @csrf

                            <div class="col-md-3">
                                <label for="">Date Range</label>
                                <input type="text" name="datetimes" class="form-control" id="">
                            </div>
                            <div class="col-md-4">
                                <br>
                                <input type="submit" value="Filter" class="btn btn-primary">
                                <a href="{{route('chats')}}" class="btn btn-danger">Clear</a>
                                
                                {{-- <input type="clear" value="clear" class="btn btn-danger"> --}}
                            </div>
                        </form>
                        <br>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>websiteId</th>
                                    <th>chats</th>
                                    <th>date</th>
                                    <th>missedChats</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($res as $item)
                                <tr>
                                    <td>{{$item['websiteId']}}</td>
                                    <td>{{$item['chats']}}</td>
                                    <td>{{$item['missedChats']}}</td>
                                    <td>{{$item['date']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('input[name="datetimes"]').daterangepicker({
                timePicker: false,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });

    </script>
</body>

</html>
