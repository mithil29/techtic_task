<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://45.79.111.106/interview.json',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        // dd(gettype($response));
        $response = json_decode($response,true);
        // $response= json_decode($response,true);
        curl_close($curl);
        // dd($response);
        
        // $res = $this->getDateWiseScore2($response);
        $res = $this->getDateWiseScore($response);
        // dd($res);
        // print_r($response);
        return view('chat',compact('res'));

    }
    
    public function getDateWiseScore($data) {
        $groups = array();
        foreach ($data as $item) {
            $date = \Carbon\Carbon::parse($item['date']);
            $date =  $date->format('Y-m-d H:i:s');
            $key = $item['websiteId'];
            if (!array_key_exists($key, $groups)) {
                $groups[$key] = array(
                    'websiteId' => $item['websiteId'],
                    'chats' => $item['chats'],
                    'date' =>$date,
                    'missedChats' => $item['missedChats'],
                );
            } else {
                $groups[$key]['chats'] = $groups[$key]['chats'] + $item['chats'];
                $groups[$key]['missedChats'] = $groups[$key]['missedChats'] + $item['missedChats'];
            }
        }
        return $groups;
    }
    public function getDateWiseScore2($data) {
        $groups = array();
        foreach ($data as $item) {
            $date = \Carbon\Carbon::parse($item['date']);
            $date =  $date->format('Y-m-d H:i:s');
            $key = $item['date'];
            if (!array_key_exists($key, $groups)) {
                $groups[$key] = array(
                    'websiteId' => $item['websiteId'],
                    'chats' => $item['chats'],
                    'date' =>$date,
                    'missedChats' => $item['missedChats'],
                );
            } else {
                $groups[$key]['chats'] = $groups[$key]['chats'] + $item['chats'];
                $groups[$key]['missedChats'] = $groups[$key]['missedChats'] + $item['missedChats'];
            }
        }
        return $groups;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        //
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://45.79.111.106/interview.json',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        // dd(gettype($response));
        $response = json_decode($response,true);
        // $response= json_decode($response,true);
        curl_close($curl);
        // dd($response);
        // dd();
        $date_filtered= explode(' - ',$request->datetimes);
        $fromdate = \Carbon\Carbon::parse($date_filtered[0]);
        $todate = \Carbon\Carbon::parse($date_filtered[1]);
        $fromdate =  $fromdate->format('Y-m-d H:i:s');
        $todate =  $todate->format('Y-m-d H:i:s');
        // $date->format('H:i:s')
       $response= array_filter( $response, function($var) use ($fromdate, $todate) {
            $utime = \Carbon\Carbon::parse($var['date']);
            $utime =  $utime->format('Y-m-d H:i:s');
            // dd($utime,$fromdate,$todate);
            $utime =strtotime($utime);
            $fromdate =strtotime($fromdate);
            $todate =strtotime($utime);
            return $utime <= $todate && $utime >= $fromdate;
         });
        // dd($response);
        $res = $this->getDateWiseScore($response);
        // dd($res);
        // print_r($response);
        return view('chat',compact('res'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
